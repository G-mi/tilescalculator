class Tile:
    def __init__(self, name, price):
        self.name = name
        self.price = price

    def __str__(self):
        return self.name


class Customer(Tile):
    def __init__(self,name, price, area):
        self.area = area

        super().__init__(name, price)

    def calculate_price(self, price, area):
        print(price*area)
        return price*area

    def __str__(self):
        return self.name